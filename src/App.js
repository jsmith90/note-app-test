import React, {Component} from 'react';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import NoteInput from './noteInput';
import Note from './Note';
import Title from './Title';
import './App.css';

class App extends Component {
  static defaultProps = {colors: ['AliceBlue', 'AntiqueWhite', 'Azure', 'Ivory', 'LemonChiffon']}
  constructor(props) {
    super(props);
    this.state = {
      notes:[],
      edit:false,
      editId:'',
      nextId:1,
      value:'',
      showForm: false,      
      iconValue: '+'
    };
    this.handleSave = this.handleSave.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
  }

  componentDidMount(){
    this.setState({
      notes:[{ id:0, body:'my first note' } ]
    })
  }
  handleSave(note) {
    if (this.state.edit){   
      this.setState(prevState => ({

        notes: prevState.notes.map(
          el => el.id === note.id? { ...el, body: note.body }: el
        ),
        edit:false
      
      }))
      
    }else{
      this.setState((prevState, props) => {      
        const newNote = {...note, id: this.state.nextId};
        return {
          nextId: prevState.nextId + 1,
          edit:false,
          notes: [...this.state.notes, newNote],
          showForm:false
        }
      });
    }
     
  }  
  onDelete(id) {
    const notes = this.state.notes.filter(r => r.id !== id);
    this.setState({
      notes,
      edit:false,
      nextId: --this.state.nextId
    });    
  }  
  onEdit(id){    
    
    this.setState({
      showForm:true,
      edit:true,
      editId: id,
      value:this.state.notes.map((val)=>(
        val.id==id ? val.body : val.body
      ))
    })    
    
  }
   
  handleClick(){    
    this.setState({showForm: !this.state.showForm, value:'', edit:false})
    this.state.iconValue === '+' ? this.setState({iconValue:'-'}):this.setState({iconValue:'+'})
  }

  render(){
    const notelist = this.state.notes.map((val, idx) =>(    //List of notes, array, list of notes
      <Note key={idx} {...val} color={this.props.colors} body={val.body} showForm={this.state.showForm} onDelete={this.onDelete} onEdit={this.onEdit} />            
    ))
    return (
      <div className="App">
        <header>
          <nav>
            <div className="nav-wrapper">
              <a href="#" className="brand-logo left">Logo</a>
              <ul id="nav-mobile" className="right">
                <li>        
                  {!this.state.showForm ?                    
                    <button className="btn" onClick={this.handleClick}><i className="material-icons">add</i></button>
                    :
                    <>
                      <button className="btn " onClick={this.handleClick}><i className="material-icons">close</i></button>
                    </>                    
                  }     
                </li>
              </ul>
            </div>
          </nav> 
        </header>
        {this.state.showForm ? <NoteInput value={this.state.value} notes={this.state.notes} edit={this.state.edit} onSave={this.handleSave} editId={this.state.editId} /> : null  }
   
        <ul className="collection">{notelist}</ul>


        <Title />
      </div>
    );
  }

}


export default App;