import React, {useRef,  Component} from 'react';
import { lineHeight } from '@material-ui/system';

class NoteInput extends Component {
    static defaultProps ={
       onSave() {}
    }
    constructor(props) {
        super(props);
        this.state = {
            body:'',
            id:''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }
    handleChange(e) {
        this.setState({body: e.target.value});      
    }
    handleClick(){    
        this.setState({showForm: !this.state.showForm})
        this.state.iconValue === '+' ? this.setState({iconValue:'-'}):this.setState({iconValue:'+'})
      }
    handleSubmit(e) {
        e.preventDefault();
        this.props.onSave({...this.state}, {...this.state.props});
        e.target.body.value = '';
        this.setState({
            body:'',            
        })
      }
    checkNotes(){
        const { value, notes, editId, edit} = this.props
        if(edit){            
            this.setState({
                body:notes[editId].body,
                id:this.props.editId
            })
            console.log(this.state)
            console.log(this.props)
        }
    }
    componentDidMount(){
        this.checkNotes();
    }
    render(){
        return(
            <form className="" onSubmit={this.handleSubmit}>
                <div className="row">
                    <div className="input-field col s12">                        
                        <textarea  id="icon_prefix2" className="materialize-textarea purple lighten-5" value={this.state.body} onChange={this.handleChange} type="textarea"  name="body" key="body" />
                        <label for="icon_prefix2" className="active">Note</label>
                    </div>
                    
                    <button className="btn waves-effect waves-light s12" type="submit" name="action">Add
                        <i className="material-icons right" style={{lineHeight:'1'}}>send</i>
                    </button>
                </div>
            </form>
        )
    }
}
export default NoteInput;