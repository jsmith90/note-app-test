import React, { Component } from 'react';
class Note extends Component { 
    constructor(props) {
      super(props);
      this.state = {
        backgroundColor: ''
      }
      this.bgChange = this.bgChange.bind(this);
    }
    
    componentDidMount(){
      this.setState({
        backgroundColor:this.props.color[0]
      })
    }
    bgChange() {
      if (this.refs.bgSelect) {
        this.setState({
          backgroundColor:this.refs.bgSelect.value
        })     
      }
    }
    render(){
      return(
        <li className="collection-item" id={this.props.noteId} style={{backgroundColor:this.state.backgroundColor}} >
          <p>{this.props.body}</p>
           {!this.props.showForm ?<a className="btn waves-effect waves-light gray" onClick={() => this.props.onEdit(this.props.id)}>edit</a> :null}
            <a className="btn-floating btn waves-effect waves-light red" onClick={() => this.props.onDelete(this.props.id)}><i className="material-icons">x</i></a>       
          <select ref="bgSelect" onChange={this.bgChange} name="colors" className="colors">
  
            {
              this.props.color.map((val, idx) =>(
                <option key={idx} value={val}>
                 {val}
                </option>
              ))
            }
  
          </select>                 
        </li>
      )
    }  
  }
  
  export default Note;