import React, { Component } from 'react';
import axios from 'axios';

class Title extends Component {    
    constructor(props){
        super(props);
        this.state={
            titleQuery:'',
            total:'',
            status:'',
            data:[],
            show:false
        }
        this.getTitles = this.getTitles.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }
    makeQuery(){
        axios.get(`https://jsonmock.hackerrank.com/api/movies/search/?Title=${this.state.titleQuery}`)             
        .then(res => {                      
            this.setState({
                status:'Searching'
                })
            const titleData = [res.data];                            
            titleData.map((value) => (
                this.setState({
                    data:[value],
                    total:value.total,
                    status:''
                })
            ))       
        })
    }
    getTitles(e){        
        e.preventDefault();                                   
         this.makeQuery();
         this.setState({
            status:''
         })          
    }
    handleChange(e){        
        this.setState({
            titleQuery:e.target.value
        })   
    }
    handleClick(){    
        this.setState({show: !this.state.show})

      }
    render(){
        const titles  = this.state.data.map((val, idx) =>(
          val.data.map((val, idx) =>(
           <li className="collection-item">{val.Title}</li>
          ))
        ))
        return(
            <>
                <button onClick={this.handleClick} className="btn">Search for titles</button>                
                {
                    this.state.show ?                
                        <div className="row">
                            <form className="input-field col s12" onSubmit={this.getTitles} >
                                <input onChange={this.handleChange} type="text" name="titles" placeholder="Type 'SpiderMan' and press Enter"></input>
                                <button className="btn">Search</button>
                            </form>
                            <span>{this.state.status}</span>
                            <ul className="collection">                    
                               
                                {titles}
                                
                            </ul>
                        </div>
                        : null                        
                }

            </>
            
        )
    }
}

export default Title;